%define _majorver 4.3
%define _fullname suite3270-%{version}
%define _fontpath /etc/X11/fontpath.d
%define _deskdir %{_datadir}/applications
%define _pngdir %{_datadir}/icons/hicolor/48x48/apps

Name:    x3270
Version: 4.3ga9
Release: 1
Summary: An IBM 3270 terminal emulator for the X Window System and Windows.
License: BSD-3-Clause AND HPND-sell-variant AND MIT AND Apache-2.0
URL:     http://x3270.sourceforge.net/
Source0: http://prdownloads.sourceforge.net/%{name}/%{_fullname}-src.tgz
Source1: x3270.desktop
Source2: x3270.png
Patch0:  x3270-3.5-ibmhostpath.patch
Patch1:  x3270-3.5-paths.patch

BuildRequires:    desktop-file-utils fontpackages-devel imake libtool
BuildRequires:    libXaw-devel ncurses-devel openssl-devel readline-devel
BuildRequires:    mkfontdir bdftopcf

Provides:  %{name}-text = %{version}-%{release}
Provides:  %{name}-x11 = %{version}-%{release}
Obsoletes: %{name}-text < %{version}-%{release}
Obsoletes: %{name}-x11 < %{version}-%{release}

%description
x3270 runs over a TELNET connection, emulating either an IBM 3279 (color) or
3278 (monochrome). It supports:

* The full TN3270E protocol
* SSL/TLS (via the OpenSSL library) for encrypted sessions
* APL2 characters
* Non-English character sets, including Russian, Turkish, Hebrew and DBCS
  Chinese and Japanese
* IND$FILE file transfer
* NVT mode (emulating a color xterm)
* A pop-up keypad for 3270-specific keys
* A scrollbar
* Printer session integration
* Extensive debugging and scripting facilities

It does not yet support graphics.

x3270 is available in several different forms:

* x3270 is for use on an X graphics display
* c3270 is a curses-based version for use on a dumb terminal (e.g.,
  a serial terminal or a Linux console)
* s3270 is a displayless version for writing screen-scraping scripts
* pr3287 is for printer emulation


%package_help

%prep
%autosetup -n suite3270-%{_majorver} -p1

for dir in c3270 pr3287 s3270 x3270; do
    for file in html; do
        mv $dir/$file $file.$dir
    done
done


%build
%configure \
    --enable-c3270 \
    --enable-pr3287 \
    --enable-s3270 \
    --enable-x3270 \
    --enable-playback
%make_build CCOPTIONS="$RPM_OPT_FLAGS" LIBX3270DIR=%{_sysconfdir}


%install
%make_install CIFONTDIR=%{_fontdir} LIBX3270DIR=%{_sysconfdir}
make install.man DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{_fontpath}
ln -sf %{_fontdir} %{buildroot}%{_fontpath}/%{name}

install -p -m 755 "$(find ./obj -type f -name playback -print -quit)" %{buildroot}%{_bindir}
install -p -m 644 playback/playback.man %{buildroot}%{_mandir}/man1/playback.1

mkdir -p %{buildroot}%{_deskdir}
desktop-file-install --dir %{buildroot}%{_deskdir} %{SOURCE1}

install -D -p -m 644 %{SOURCE2} %{buildroot}%{_pngdir}/%{basename:%{SOURCE2}}

%files
%doc README.* html.*
%license wc3270/LICENSE.*
%config(noreplace) %{_sysconfdir}/ibm_hosts
%{_deskdir}/x3270.desktop
%{_pngdir}/x3270.png
%{_fontdir}/
%{_fontpath}/x3270
%{_bindir}/*

%files help
%{_mandir}/man*/*


%changelog
* Thu Aug 08 2024 Funda Wang <fundawang@yeah.net> - 4.3ga9-1
- update to 4.3ga9

* Sat Sep 02 2023 Ge Wang <wang__ge@126.com> - 4.2ga10-1
- Update to version 4.2

* Tue Aug 03 2021 linjiaxin5 <linjiaxin5@huawei.com> - 3.6ga5-6
- Fix failure caused by GCC upgrade to 10

* Fri Jul 30 2021 linjiaxin5 <linjiaxin5@huawei.com> - 3.6ga5-5
- Fix failure caused by GCC upgrade to 10

* Sat Nov 30 2019 huyan <hu.huyan@huawei.com> - 3.6ga5-4
- Package Initialization
